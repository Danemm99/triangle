def is_triangle(a, b, c):
    import math
    if a > 0 and b > 0 and c > 0:
        if (a + b > c) and (b + c > a) and (c + a > b):

            angle_a = math.degrees(math.acos((b**2 + c**2 - a**2) / (2 * b * c)))
            angle_b = math.degrees(math.acos((c**2 + a**2 - b**2) / (2 * c * a)))
            angle_c = 180 - angle_a - angle_b

            if math.isclose(angle_a + angle_b + angle_c, 180, rel_tol=1e-9):
                return True

    return False
